__author__ = 'Janno Siim'

'''Contains operations for sparse matrices over binary field.

Sparse matrix data structure:
    pair (rows, cols) where rows[i][j] = k if in the matrix i-th row contains
    one in column k and similarly cols[i][j] = k if column i contains one in the k-th row.
    rows[i] and cols[i] are sorted.
'''

'''
Elementary matrix operation of row swapping.
Swaps i-th and j-th row.
'''
def swap_rows(i, j, rows, cols):
    if i == j:
        return

    #swap rows
    row_i = rows[i]
    rows[i] = rows[j]
    rows[j] = row_i

    #fix columns
    for k in rows[i]:
        for l in range(len(cols[k])):
            if cols[k][l] == j:
                cols[k][l] = i
                cols[k] = sorted(cols[k])
                break

    for k in rows[j]:
        for l in range(len(cols[k])):
            if cols[k][l] == i:
                cols[k][l] = j
                cols[k] = sorted(cols[k])
                break
    return

'''
Elementary matrix operation of adding one row to another.
Adds j-th row to the i-th row.
'''
def add_rows(i, j, rows, cols):

    #add rows
    new_row_i = []
    l = 0
    for a in rows[j]:
        while l < len(rows[i]) and a > rows[i][l]:
            new_row_i.append(rows[i][l])
            l+=1
        if l >= len(rows[i]) or a != rows[i][l]:
            new_row_i.append(a)
            cols[a].append(i)
            #perhaps everything does not have to be sorted?
            cols[a] = sorted(cols[a])
        else:
            cols[a].remove(i)
            l+=1

    for k in range(l, len(rows[i])):
        new_row_i.append(rows[i][k])
    rows[i] = new_row_i

    return


'''
Solves linear system of equation over binary field.
Input: coefficient matrix as rows and columns, vector of constants in system of equations.
Returns solution vector or None if system is inconsistent or does not have a unique solution.
'''
def solve_linear_system(coefficient_rows, coefficient_cols, constant_vec):
    #No unique solution
    if len(coefficient_rows) < len(coefficient_cols):
        return None

    for k in range(min(len(coefficient_rows), len(coefficient_cols))):

        #find the k-th pivot
        i_max = next((i for i in coefficient_cols[k] if i >= k), None)
        if i_max == None:
            return None #No unique solution

        swap_rows(i_max, k, coefficient_rows, coefficient_cols)
        temp = constant_vec[i_max]
        constant_vec[i_max] = constant_vec[k]
        constant_vec[k] = temp

        #Make column below pivot to zeros
        size = len(coefficient_cols[k])
        i = 0
        while i < size:
            row_nr = coefficient_cols[k][i]
            if row_nr >= k+1:
                add_rows(row_nr, k, coefficient_rows, coefficient_cols)
                constant_vec[row_nr] ^= constant_vec[k]
                size -= 1
            else:
                i += 1

    #Consistency check
    for i in range(len(coefficient_cols), len(coefficient_rows)):
        if constant_vec[i] != 0:
            return None

    #Back substitution
    solution = [None]*len(coefficient_cols)
    for i in range(len(coefficient_cols)-1,-1,-1):
        value = constant_vec[i]

        for j in range(1, len(coefficient_rows[i])):
            value ^= solution[coefficient_rows[i][j]]

        solution[i] = value


    return solution
